# Project1-Focus
Call method in dialer.js

Krok 1 : Odpalamy konsolę linuxową, lub program imitujący typu cmder.
         Korzystając z komendy "cd" podajemy ścieżkę  do której chcemy zapisać plik np:
         "cd Plik/następnyPlik" a następnie komendą 
         "git clone https://Krawiec97@bitbucket.org/Krawiec97/node-angularproject.git"
         pobieramy projekt na swój komputer.
         
Krok 2:  Wchodzimy do folderu głównego komendą "cd" i następnie instalujemy wszystkie
         potrzebne biblioteki korzystając z komendy "npm install".
         
Krok 3:  Wpisujemy komendę "node dialer.js" (spodziewamy się komunikatu : "app listening on port 3000")

Krok 4: Odpalamy przeglądarkę, wchodzimy na stronę "https://restninja.io" i pilnujemy aby:
        Pod logiem strony ustawiamy metodę POST, po prawo uzupełniamy : "http://localhost:3000/call".
        Poniżej w sekcji headers: "content-type" a w value: "application/json" . 
        Przechodzimy do sekcji body i wpisujemy :
        "
        {
        "number1": "(Tutaj wpisujemy numer w formacie xxxxxxxxx)",
        "number2": "(Tutaj wpisujemy numer w formacie xxxxxxxxx)"
        }
        "
        Aby wykonać połączenie, klikamy "Send" na ielonym tle w prawym górnym rogu strony.
        
Krok 5: Jeśli chcemy sprawdzić status połączenia w przeglądarce wpisujemy : "http://localhost:3000/status".

I w ten oo sposób możemy się cieszyć błyskawicznym połączeniem :)
         
