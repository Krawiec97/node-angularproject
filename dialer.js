const express = require('express');
const app = express();
const Dialer = require('dialer').Dialer;
const cors = require('cors');
const bodyParser = require('body-parser');
const http = require('http').Server(app);
const io = require('socket.io')(http);

const config = {
 url: 'https://uni-call.fcc-online.pl',
 login: 'focus02',
 password: 'y234wfhg2k'
};

app.use(cors());
app.use(bodyParser.json());

Dialer.configure(config);

http.listen(3000, () => {
 console.log('app listening on port 3000');
});

io.on('connection', (socket) => {
 console.log('user connected');
 socket.on('disconnect', () => {
  console.log('user disconnected');
 });
 socket.on('message', (message) => {
  console.log('message', message)
 });
 socket.on('status', (status) => {
  console.log('status', status)
 });
 io.emit('message', 'connected!');

});

let bridge;
let currentStatus;

app.post('/call/', async (req, res) => {
 const body = req.body;
 bridge = await Dialer.call(body.number1, body.number2);
 let interval = setInterval(async () => {
  let status = await bridge.getStatus();
  if(currentStatus !== status) {
   currentStatus = status;
   io.emit('status', status);
  }}, 500);
 res.json({success: true});
});

app.get('/status', async (req, res) => {
 if(bridge !== null && bridge !== undefined) {
  let status = await bridge.getStatus();
  res.json({success: true, status: status});
 } else {
  res.json({success: true, status: "NOT CONNECTED"});
 }
});